from TP2 import *
def test_creation_box():
    b= box()
    
def test_box_add():
    b = box()
    b.add("truc1")
    b.add("truc2")
    assert "truc1" in b
    assert "truc2" in b
    assert "truc3" not in b

def test_box_open():
     b = box()
     assert b.is_open()
     b.close()
     assert not(b.is_open())
     b.open()
     assert b.is_open()
     
def test_box_suppr():
    b = box()
    b.add("truc1")
    b.add("truc2")
    b.suppr("truc1")
    assert "truc2" in b
    assert "truc1" not in b
    
def test_box_look():
    b = box()
    b2 = box()
    b.add("ceci")
    b.add("cela")    
    assert b.action_look() == "la boite contient: ceci, cela"
    assert b2.action_look() == "la boite est fermee"
    
def test_Thing():
    t1=Thing(3)
    t2=Thing(4)
    assert t1.volume() == 3
    assert t2.volume() == 4
    
def test_box_capacite():
    b = box()
    b2 = box()
    b.set_capacity(5)
    b2.set_capacity(8)
    assert b.capacity() == 5
    assert b2.capacity() == 8
    b3 = box()
    assert b3.capacity() == None
    
def place_libre_box():
    b = box()
    b.set_capacity(10)
    t1=Thing(3)
    t2=Thing(3)
    t3=Thing(4)
    t4=Thing(5)
    b.add(t1)
    b.add(t2)
    assert b.has_room_for(t3) == True
    assert b.has_room_for(t4) == False
    